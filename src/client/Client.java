package client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Timo on 14.12.2015.
 */
public class Client extends Thread {

    // ------------------------------------------ declerating web vars -------------------------------------
    private String host = "localhost";
    private int port = 4000;

    // ------------------------------------------ declerating io vars --------------------------------------
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    // ------------------------------------------------------------------------------------------------------

    /**
     *
     */
    public Client() {
        try {
            // =========================================
            initClient();
            sendMessage("TEST");

            // =========================================
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("ERROR 100: IO EXCEPTION");
        } finally {
            try {
                bufferedWriter.close();
                bufferedWriter.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("ERROR 101: SOCKET NOT CLOSED");
            }
        }
    }

    /**
     *
     */
    public void run() {

    }

    /**
     * @throws IOException
     */
    private void initClient() throws IOException {
        socket = new Socket(InetAddress.getByName(host), port);
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    /**
     * @param message
     * @throws IOException
     */
    private void sendMessage(String message) throws IOException {
        bufferedWriter.write(message + "\n");
        bufferedWriter.flush();
    }

    /**
     * @return
     * @throws IOException
     */
    private String getMessage() throws IOException {
        String message = bufferedReader.readLine();
        return message;
    }


}
